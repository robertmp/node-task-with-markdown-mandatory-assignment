# Node task

This project contains node hello-world

## Installation steps

1. Copy the repository
2. Install dependencies
3. Start the app

## Example of terminal

Code example: 

```sh
mkdir node-task
cd ./node-task
npm i express
```

<h2>Checklist</h2>

- [x] Git ignore
- [ ] Check dependencies
- [ ] Organize code


### Done
![alt text](https://www.memeatlas.com/images/pepes/pepe-floating-up-balloons.gif)